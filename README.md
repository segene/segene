SowerPHP
========

SowerPHP es un framework, o ambiente de desarrollo, para PHP.

El framework es minimalista, entregando las herramientas básicas para el
desarrollo. Además existen extensiones oficiales que entregan nuevas
funcionalidades y cualquier usuario puede desarrollar sus propias extensiones.

El objetivo principal del framework es entregar una estructura estándar y mínima
que cualquier aplicación en PHP debería considerar utilizando los patrones y
buenas prácticas que el equipo de SowerPHP considera.

Todo el código generado por el equipo de SowerPHP se encuentra liberado
utilizando la licencia GPL v3 o superior.

Enlaces de interés
------------------

* Página del proyecto: <http://sowerphp.org>
* Documentación: <http://sowerphp.org/doc>
* Proyecto en GitHUB: <https://github.com/SowerPHP/sowerphp>
